var message = require('./models/messageModel')
var chat = require('./models/chatModel')
var users = require('./models/userModel')
var block = require('./models/blockModel')
var feedback = require('./models/feedbackModel')
var student = require('./models/studentModel')
// var async = require('async')
var mongoose = require('mongoose')
var ObjectId = mongoose.Types.ObjectId
var FCM = require('fcm-push');


module.exports = function (app, socket, clients, firebase) {

    var serverKey = 'AAAADicj7PU:APA91bGRLTa8ZzIJiKLqeU2IFVE_Haf2tyDXX1e1mvAC3kNDIER7xzeswf95I8VKwZlXqirm7ABn5KQkN0YOC50v72F1V5f7f6W0dnCRP_yOXlPE_fbh5lVf_EntAHs6u7uYHD4Gg6gs';
    var fcm = new FCM(serverKey);

    // ------------------------ chat model ----------------------------

    app.post('/api/new-chat', function (req, res) {
        var new_chat = new chat({
            participants: req.body.participants,
            name: req.body.name,
            receiver_role: req.body.receiver_role,
            sender_role: req.body.sender_role
        })


        // console.log(req.body)

        new_chat.save(function (err, data) {
            if (err) { }
            else {
                req.body.participants.forEach(function (id) {
                    if (clients[id] != undefined) {
                        clients[id].join(data.id)
                    }
                });
                socket.broadcast.emit('new-chat', data)



                // socket.broadcast.to(data.participants[1]).emit('new-chat', data)

                let a = data.participants.indexOf(req.body.authorid)
                data.participants.splice(a, 1)

                const receiver_id = data.participants[0]
                // console.log('1')
                if (clients[receiver_id] != undefined) {
                    // console.log('2')
                    clients[receiver_id].emit('current-chat', { authorid: req.body.authorid, chat_id: data.id })
                }

                // console.log(clients[req.body.receiver_id])
                if (clients[req.body.receiver_id] != undefined) {
                    // console.log('3')
                    clients[req.body.receiver_id].emit('new-chat-setState', { chat_id: data.id, authorid: req.body.authorid })
                }




                res.send(data)
            }
        })
    })


    //  old query

    // app.get('/api/chat-list/:authorid', function (req, res) {
    //     let option = {
    //         options: { sort: { ts: -1 } },
    //         // match: { read: false },
    //     }

    //     const authorid = new ObjectId(req.params['authorid'])
    //     chat.find({
    //         participants: authorid
    //     }).populate({
    //         path: 'messages',
    //         ...option
    //     }).populate({
    //         path: 'participants',
    //         match: { _id: { $ne: authorid } }
    //     }).then(data => {
    //         let list = []
    //         async.mapSeries(data, function (item, func) {
    //             let a = item.participants.pop()
    //             list.push(a)
    //             item.participants[0] = a.id

    //             if (item.messages.length === 0) {
    //                 message.find({ chat_id: item.id }).sort({ 'ts': -1 }).limit(10).exec(function (err, msgs) {
    //                     item.messages = msgs
    //                     return func(null, item)
    //                 })
    //             } else {
    //                 return func(null, item)
    //             }

    //         }, function (err, data) {
    //             res.send({ data, list })
    //         })

    //     })
    // })


    //  new query


    app.get('/api/chat-list/:authorid', function (req, res) {
        // let option = {
        //     options: { sort: { ts: -1 } },
        //     // match: { read: false },
        // }
        // console.log(req.params['authorid'], '- - - - -')
        const authorid = new ObjectId(req.params['authorid'])

        chat.find({
            participants: authorid
        }).populate('messages')
            .populate({
                path: 'participants',
                match: { _id: { $ne: authorid } }
            }).sort({ 'updated': -1 }).then(data => {
                let list = []

                data.forEach(function (item) {
                    let a = item.participants.pop()
                    list.push(a)
                    item.participants[0] = a.id
                })
                res.send({ data, list })

                // async.mapSeries(data, function (item, func) {
                //     let a = item.participants.pop()
                //     list.push(a)
                //     item.participants[0] = a.id

                //     message.count({ chat_id: item.id, author: { $ne: authorid }, read: false }).exec(function (err, count) {

                //         if (count === 0) {
                //             message.find({ chat_id: item.id }).sort({ 'ts': -1 }).limit(15).exec(function (err, msgs) {
                //                 item.messages = msgs
                //                 item.count = count
                //                 return func(null, item)
                //             })
                //         } else {
                //             message.find({
                //                 chat_id: item.id,
                //                 author: { $ne: authorid },
                //                 read: false
                //             }).sort({ ts: 1 }).limit(1).exec(function (err, msg) {

                //                 if (msg.length > 0) {
                //                     message.find({
                //                         chat_id: item.id,
                //                         _id: { $gte: msg[0].id }
                //                     }).limit(8).exec(function (err, data2) {
                //                         message.find({
                //                             chat_id: item.id,
                //                             _id: { $lt: msg[0].id }
                //                         }).limit(8).exec(function (err, data3) {
                //                             item.messages = [...data3, ...data2].reverse()
                //                             item.count = count
                //                             // console.log(item)
                //                             return func(null, item)
                //                         })
                //                     })

                //                 }

                //             })
                //         }
                //     })


                // }, function (err, data) {
                //     console.log(data, 'data...')
                //     data[0].messages.forEach(function (d) {
                //         console.log(d)
                //     })
                //     res.send({ data, list })
                // })

            })
    })







    // app.get('/api/chat-list-buddy/:authorid', function (req, res) {

    //     const authorid = new ObjectId(req.params['authorid'])
    //     chat.find({
    //         participants: authorid
    //     }).exec(function (err, data) {

    //         var studentList = []

    //         data.forEach(function (item) {

    //             let a = item.participants.indexOf(authorid)
    //             item.participants.splice(a, 1)
    //             // console.log(item.participants[0])
    //             studentList.push(item.participants[0])
    //         })

    //         users.find({ '_id': { '$in': studentList } }, function (err, stdData) {
    //             res.send({ data, stdData })
    //         })


    //     })
    // })

    app.get('/api/delete-chat/:id', function (req, res) {
        // //console.log(req.params['id'])
        chat.findByIdAndDelete(req.params['id'], function (err, data) {
            res.send(data)
        })
    })


    // --------------------- message model -------------------------------

    app.get('/api/get-chat/:chatid', function (req, res) {
        message.find({ chat_id: new ObjectId(req.params['chatid']) }, function (err, data) {
            res.send(data)
        })
    })

    app.post('/api/new-msg', function (req, res) {
        // console.log('new msg')
        var new_message = new message({
            msg: req.body.msg, // group or personal
            author: req.body.author,
            chat_id: req.body.chat_id,
            ts: req.body.ts,
            sent: true
        })
        new_message.save(function (err, data) {

            chat.findByIdAndUpdate(req.body.chat_id, { updated: req.body.ts }, { new: true }).exec(function (err, data) {
                // console.log('- - - - -', data)
            })
            // console.log('- - - - -', req.body)
            socket.broadcast.to(req.body.chat_id).emit('msg-receieve',
                { data, chatId: req.body.chat_id, role: req.body.role, sender_role: req.body.sender_role, uid: req.body.id }
            )

            users.findById(req.body.receiver_id, function (err, userData) {

                var message = {
                    to: userData.fcm_token,
                    content_available: true,
                    data: {
                        chat_id: req.body.chat_id,
                        receiver_id: req.body.author,
                        receiver_role: req.body.role,
                        previous_screen: "Dashboard",
                        sender_role: req.body.sender_role,
                        refresh: 'this.refreshComponent',
                        name: req.body.name,
                        body: req.body.msg.msg
                    },
                    priority: "high",
                };

                if (userData.deviceType != 'android') {
                    message['notification'] = {
                        title: req.body.name,
                        body: req.body.msg.msg,
                        sound: "default",
                        vibration: "default"
                    }

                };

                // console.log('new saved', err, req.body.msg, req.body.name)

                if (userData.notification) {
                    fcm.send(message, function (err, response) {
                        if (err) {
                            console.log("Something has gone wrong ! ", err, response);
                        } else {
                            console.log("Successfully sent with resposne : ", response);
                        }
                    });
                }

            })

            res.send({ data, uId: req.body.id })
        })
    })

    app.get('/api/getNextMessages/:lastmsgId/:chatid', function (req, res) {

        message.find({
            chat_id: req.params['chatid'],
            _id: { $gt: req.params['lastmsgId'] }
        }).sort({ ts: -1 }).limit(5).exec(function (err, data) {
            res.send(data)
        })
    })

    app.get('/api/getPrevMessages/:lastmsgId/:chatid', function (req, res) {
        message.find({
            chat_id: req.params['chatid'],
            _id: { $lt: req.params['lastmsgId'] }
        }).sort({ ts: -1 }).limit(5).exec(function (err, data) {
            res.send(data)
        })
    })

    // app.put('/api/delivered/:msgid', function (req, res) {
    //     message.findByIdAndUpdate(
    //         req.params['msgid'],
    //         { delivered: req.body['receiverId'] },
    //         function (err, data) {
    //             res.send(data)
    //         }
    //     )
    // })

    app.put('/api/read/:receiverid', function (req, res) {
        // console.log(req.body, 'read')
        message.update({ _id: { '$in': req.body.msgIdList }, author: req.params['receiverid'] },
            { read: true }, { multi: true },
            function (err, data) {

                socket.broadcast.to(req.body.chat_id).emit('read-msg',
                    {
                        data: req.body.msgIdList,
                        chat_id: req.body.chat_id,
                        role: req.body.role,
                        author: req.body.author,
                        sender_role: req.body.sender_role
                    })
                res.send(data)
            }
        )
    })

    app.put('/api/deletemsg/:msgid/:userid', function (req, res) {
        // console.log('called........', req.body)
        message.findByIdAndUpdate(
            req.params['msgid'],
            { '$push': { is_deleted: req.params['userid'] } },
            { "new": true, "upsert": true }
        ).exec(function (err, data) {
            // socket.broadcast.to(data.chat_id).emit('delete-msg', { chat_id: data.chat_id, data, role: req.body.role })
            res.send(data)
        })
    })


    // ------------------------- user model -----------------------------------------

    app.post('/api/login', function (req, res) {
        users.findOne({
            name: req.body['username']
        }, function (err, data) {

            if (data === null) {

                res.send({ 'error': true })
            } else {
                res.send(data)
            }

        })
    })

    app.get('/api/all-users/:currentUserId', function (req, res) {
        const currentUserId = new ObjectId(req.params.currentUserId)
        users.find({ _id: { $ne: currentUserId } }, function (err, data) {
            res.send(data)
        })
    })

    app.get('/api/findUser/:id', function (req, res) {
        users.findById(req.params['id'], function (err, data) {
            res.send(data)
        })

    })

    app.put('/api/notification', function (req, res) {
        users.findOneAndUpdate(
            { _id: new ObjectId(req.body.id) },
            { notification: req.body.val },
            { new: true, upsert: true },
            function (err, data) {
                // console.log(data, err, ' - - - - - -')
                res.send(data)
            })
    })

    app.get('/api/clear-fcm/:id', function (req, res) {
        // console.log(req.params['id'], '===============================================================')
        users.findOneAndUpdate({ _id: new ObjectId(req.params['id']) }, { fcm_token: '' }, { new: true },
            function (err, data) {
                // console.log(err)
                res.send(data)
            })
    })


    // app.put('/api/updateAppState', function (req, res) {
    //     users.findByIdAndUpdate(req.body.user_id,
    //         { appState: req.body.app_state },
    //         { upsert: true },
    //         function (err, data) {
    //             res.send(data)
    //         })
    // })

    // ----------------------------- block model -------------------------------------
    app.get('/api/getblockDetails/:receiverId/:senderId', function (req, res) {
        const params = req.params
        block.findOne({ self_id: new ObjectId(params['receiverId']), user_id: new ObjectId(params['senderId']) }, function (err, data) {
            block.findOne({ self_id: new ObjectId(params['senderId']), user_id: new ObjectId(params['receiverId']) }, function (err2, data2) {
                // console.log(data2, 'blocked_by_me')
                res.send({
                    data,
                    data2
                })
            })
        })
    })

    app.put('/api/blockUnblock', function (req, res) {
        block.findOneAndUpdate({ self_id: req.body.senderId, user_id: req.body.receiverId },
            { is_blocked: req.body.is_blocked },
            { new: true, upsert: true },
            function (err, data) {
                // console.log(err, data)
                socket.broadcast.to(req.body.chat_id).emit('block',
                    {
                        chat_id: req.body.chat_id,
                        senderId: req.body.senderId,
                        is_blocked: data.is_blocked
                    })
                res.send(data)
            })
    })


    // --------------- feedback ------------------------------------------------

    app.post('/api/feedback', function (req, res) {
        // console.log(req.body)
        var feedBack = new feedback({
            msg: req.body.msg,
            name: req.body.name,
            email: req.body.email
        })
        feedBack.save(function (err, data) {
            // console.log(data)
            res.send(data)
        })
    })

    // ----------------------------- socket-------------------------------------

    socket.on('ol-status', (recData) => {

        if (recData) {
            users.findOneAndUpdate({ _id: new ObjectId(recData) },
                { lastSeen: true },
                { new: true, upsert: true },
                function (err, data) {
                    socket.broadcast.emit('status-changed', data)
                    // console.log('ol-status', data)
                })

            clients[recData] = socket

        }
    });

    socket.on('ol-status-offline', (Data) => {

        users.findOneAndUpdate({ _id: new ObjectId(Data.id) }, { lastSeen: Data.ts }, { new: true },
            function (err, data) {
                socket.broadcast.emit('status-changed', data)
                // console.log('ol-status-offline', data)
                delete clients[Data.id]
            })
    });

    socket.on('create-room', (id) => {
        // //console.log(id, 'id')
        socket.join(id)
    });

    socket.on('create-room-array', (ids) => {
        // console.log('create-room-array', ids)
        ids.forEach(function (id) {
            socket.join(id)
        })

    });

    socket.on('typing', function (chatId) {
        // console.log('typing', chatId)
        socket.broadcast.to(chatId).emit('status-changed-typing', chatId)
    })

    socket.on('no-typing', function (chatId) {
        // console.log('no-typing', chatId)
        socket.broadcast.to(chatId).emit('status-changed-no-typing', chatId)
    })

    socket.on('typing-before', function (data) {

        if (clients[data.receiver_id]) {
            // console.log(data, '--', clients)
            data._id = { $oid: data.id }
            clients[data.receiver_id].emit('status-changed-typing-before', data)
        }
    })

    socket.on('no-typing-before', function (data) {
        if (clients[data.receiver_id]) {
            data._id = { $oid: data.id }
            clients[data.receiver_id].emit('status-changed-no-typing-before', data)
        }
    })

    socket.on('update-read-status', function (recData) {
        // console.log(recData, 'abc')
        message.update({ _id: recData.msgId }, { read: true }, function (err, data) {
            // console.log(recData, 'err', err)
            socket.broadcast.to(recData.chat_id).emit('read-msg',
                {
                    data: recData.msgId,
                    chat_id: recData.chat_id,
                    role: recData.role,
                    author: data.author,
                    sender_role: recData.sender_role
                })
        }
        )
    })



    socket.on('replace-array', function (id) {
        clients[id] = socket
    })

    socket.on('disconnect', function () {
        let keys = Object.keys(clients)
        keys.forEach(function (key) {

            if (clients[key].id == socket.id) {
                console.log('matched')
                users.findOneAndUpdate({ _id: new ObjectId(key) }, { lastSeen: false }, { new: true },
                    function (err, data) {
                        console.log('done..')
                        socket.broadcast.emit('status-changed', data)
                        delete clients[key]
                    })

            }
        })

        console.log('disconnected')
    })


    socket.on('create-new-user', function () {

        // for (var i = 1; i < 101; i++) {

        //     var user1 = new users({
        //         lastSeen: true,
        //         username: `new${i}@dev.tz`,
        //         password: '123',
        //         first_name: `New ${i}`,
        //         last_name: 'dev',
        //         sex: 'male',
        //         about: "",
        //         country_code: "+91",
        //         contact: `${Math.floor(Math.random() * 9000000000) + 1000000000}`,
        //         email: `new${i}@dev.tz`,
        //         home_address: "",
        //         city: "",
        //         origin: "India",
        //         country: "",
        //         university: "",
        //         date_of_birth: new Date("2018-12-12 15:25:12.296"),
        //         mother_tongue: "",
        //         languages: ["Hindi", "English"],
        //         image: "/static/image/default_user.png",
        //         education: "",
        //         employement: "",
        //         token: "",
        //         login_type: "knowhassles",
        //         is_buddy: false,
        //         is_active: true,
        //         is_contact_verified: true,
        //         roles: ['kh_student'],
        //         home_town: "",
        //         current_city: "",
        //         current_country: "",
        //         created: new Date("2018-12-12 15:25:12.296"),
        //         updated: new Date("2018-12-12 15:25:12.296"),
        //         otp_valid_till: new Date("2018-12-12 15:25:12.296"),
        //         sms_otp: 1245,
        //         fcm_token: '',
        //         is_deleted: false,
        //         deviceType: 'android',
        //         notification: true
        //     })

        //     user1.save(function (err, data) {
        //         console.log('new user ----> ', err, data.id)
        //         var student1 = new student({
        //             user_id: data.id,

        //             arrival_city: "Welland",
        //             arrival_country: "Canada",
        //             arrival_province: "Ontario",

        //             air_ticket: "",
        //             passport_img: "",
        //             visa_img: "",
        //             arrival_dt: new Date("2018-12-12 15:25:12.296"),
        //             arrival_terminal: "",
        //             departure_dt: new Date("2018-12-12 15:25:12.296"),
        //             departure_terminal: "",
        //             flight_number: "",

        //             info_exists: true,
        //             student_availability: [],
        //             buddy_availability: [],

        //             bookmarked_buddies: [],
        //             bookmarked_students: [],

        //             services: [],

        //             university: "",
        //             visible: true,
        //             created: new Date(),
        //             updated: new Date(),
        //             travelling_month: "Sep",
        //             travelling_year: "2020"
        //         })

        //         student1.save(function (error, d) {
        //             // console.log(err, d)
        //         })
        //     })
        // }

    })
}