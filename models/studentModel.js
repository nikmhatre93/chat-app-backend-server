var mongoose = require('mongoose')

var student = mongoose.Schema({
    user_id: { type: String, default: "" },

    arrival_city: { type: String, default: "" },
    arrival_country: { type: String, default: "" },
    arrival_province: { type: String, default: "" },

    air_ticket: { type: String, default: "" },
    passport_img: { type: String, default: "" },
    visa_img: { type: String, default: "" },
    arrival_dt: { type: Date, default: "" },
    arrival_terminal: { type: String, default: "" },
    departure_dt: { type: Date, default: "" },
    departure_terminal: { type: String, default: "" },
    flight_number: { type: String, default: "" },

    info_exists: { type: Boolean, default: true },
    student_availability: { type: Array, default: [] },
    buddy_availability: { type: Array, default: [] },

    bookmarked_buddies: { type: Array, default: [] },
    bookmarked_students: { type: Array, default: [] },

    services: { type: Array, default: [] },

    university: { type: String, default: "" },
    visible: { type: Boolean, default: true },
    created: { type: Date, default: Date.now() },
    updated: { type: Date, default: Date.now() },
    travelling_month: { type: String, default: "" },
    travelling_year: { type: String, default: "" }
},
    { collection: 'student', versionKey: false })

module.exports = mongoose.model("student", student);
