var mongoose = require('mongoose')

var user_lists = mongoose.Schema({
    lastSeen: { type: 'Mixed' },
    username: { type: String, default: "" },
    password: { type: String, default: "" },
    first_name: { type: String, default: "" },
    last_name: { type: String, default: "" },
    sex: { type: String, default: "" },
    about: { type: String, default: "" },
    country_code: { type: String, default: "" },
    contact: { type: String, default: "" },
    email: { type: String, default: "" },
    home_address: { type: String, default: "" },
    city: { type: String, default: "" },
    origin: { type: String, default: "" },
    country: { type: String, default: "" },
    university: { type: String, default: "" },
    date_of_birth: { type: Date },
    mother_tongue: { type: String, default: "" },
    languages: { type: Array },
    image: { type: String, default: "" },
    education: { type: String, default: "" },
    employement: { type: String, default: "" },
    token: { type: String, default: "" },
    login_type: { type: String, default: "" },
    is_buddy: { type: Boolean },
    is_active: { type: Boolean },
    is_contact_verified: { type: Boolean },
    roles: { type: Array },
    home_town: { type: String, default: "" },
    current_city: { type: String, default: "" },
    current_country: { type: String, default: "" },
    created: { type: Date },
    updated: { type: Date },
    otp_valid_till: { type: Date },
    sms_otp: { type: Number },
    fcm_token: { type: String, default: "" },
    is_deleted: { type: Boolean },
    deviceType: { type: String, default: '' },
    notification: { type: Boolean, default: true }
},
    {
        toObject: {
            transform: function (doc, ret) {
                ret._id = { $oid: ret._id };
                ret.created = { $date: ret.created };
                // delete ret._id;
                return ret
            }
        },
        toJSON: {
            transform: function (doc, ret) {
                ret._id = { $oid: ret._id };
                ret.created = { $date: ret.created };
                // delete ret._id;
                return ret
            }
        }
    })

module.exports = mongoose.model("user", user_lists);
// module.exports = mongoose.model("user_lists", user_lists);