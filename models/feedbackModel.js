var mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId

var feedback = mongoose.Schema({
    name: { type: String, default: "" },
    email: { type: String, default: "" },
    msg: { type: String, default: "" },
    created: { type: Date, default: Date.now }
}, {
        toObject: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        },
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                delete ret._id;
                return ret
            }
        }
    })


module.exports = mongoose.model("feedback", feedback);